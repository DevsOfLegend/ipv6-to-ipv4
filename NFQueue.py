#!/usr/bin/python
# coding=utf-8

import nfqueue, socket;
from scapy.all import *;
import logging;

__LOGGING__ = logging.INFO;
__LOGGING__ = logging.WARNING;

__TCP_PORT__ = 80;

__TCP_RELAY__ = {};
__ASSOC__ = {};#!< Association @IPv4 -> @IPv6
__REAL_DNS__ = '8.8.8.8';

__MAC_HOTE__ = '00:0a:ba:be:ca:fe';


def traite_paquet(payload):
	logging.info('IN traite_paquet');

	# le paquet est fourni sous forme d’une séquence d’octet, il faut l’importer
	data = payload.get_data();
	# il faut identifier sa nature IPv6 ou IPv4
	premier_quartet = data[0].encode("hex")[0];

	# le paquet est rejeté, on s'est chargé de l'envoyer nous-même avec scapy
	payload.set_verdict(nfqueue.NF_DROP);

	# paquet IPv4 : serveur externe -> hote1
	if premier_quartet == '4':
		logging.info('IPv4 detected');
		pkt = IP(data);

		########################################################################
		# DNS paquet
		if DNS in pkt :
			domain = pkt.qd.qname;
			logging.info('DNS detected for '+domain);
			if not pkt.an:
				logging.info('>> DROP');
				return ;

			return ;

		########################################################################
		# TCP paquet
		elif TCP in pkt:
			logging.info('TCP detected <= to IPv6');
			ipv4_src = pkt.src;

			if ipv4_src+str(pkt.sport) not in __TCP_RELAY__:
				logging.info('Destination inconnue : '+str(pkt.dst));
				return ;

			ipv6_info = __TCP_RELAY__[ipv4_src+str(pkt.sport)];

			logging.info('>> Dest='+str(ipv6_info));

			# On récupère la partie TCP
			pkt_tcp = pkt[TCP];
			pkt_tcp.dport = ipv6_info['sport'];
			del pkt_tcp.chksum;

			pk_final = Ether(dst=__MAC_HOTE__)/IPv6(dst=ipv6_info['ip_src'], src=__ASSOC__[pkt.src])/pkt_tcp;
			del pk_final[IPv6].chksum;
			del pk_final[TCP].chksum;

			sendp(pk_final, iface="bridge_ipv6", verbose=__LOGGING__ == logging.INFO);


	############################################################################
	# IPv6 : hote1 -> serveur externe
	else:
		logging.info('IPv6 detected');
		pkt = IPv6(data);

		########################################################################
		# DNS paquet
		if DNS in pkt:
			domain = pkt.qd.qname;
			logging.info('DNS detected Query for '+domain);

			if pkt.qd.qtype == 1:# 'A'
				logging.info('>> Illegal resolv for IPv4 !');
				return;

			logging.info('>> Send DNS Query');
			# Création de deux requetes DNS vers le serveur externe pour obtenir son IPv4 et son IPv6
			# Envoi des deux requetes DNS au server distant
			dns_A = sr1(IP(dst=__REAL_DNS__)/UDP(dport=53)/DNS(rd=1, qd=DNSQR(qname=domain, qtype='A')), iface='eth0', verbose=__LOGGING__ == logging.INFO);
			dns_AAAA = sr1(IP(dst=__REAL_DNS__)/UDP(dport=53)/DNS(rd=1, qd=DNSQR(qname=domain, qtype='AAAA')), iface='eth0', verbose=__LOGGING__ == logging.INFO);

			ipv4 = dns_A.an[0].rdata;
			logging.info('>> ipv4='+ipv4);
			ipv6 = dns_AAAA.an[0].rdata;
			logging.info('>> ipv6='+ipv6);

			__ASSOC__[ipv4] = ipv6;

			sendp(
				Ether(dst=__MAC_HOTE__)/IPv6(dst=pkt.src, src=pkt.dst)/
				UDP(dport=pkt.sport,sport=53)/
				DNS(rd=1, id=pkt.id,
					qd=DNSQR(qname=domain, qtype='AAAA'),
					an=DNSRR(rrname=domain, type='AAAA', rdata=ipv6)
				)
			,iface='bridge_ipv6', verbose=__LOGGING__ == logging.INFO);


		########################################################################
		# TCP paquet
		elif TCP in pkt:
			logging.info('TCP detected => to IPv4');
			ipv4_dst = translate_IPv6_to_IPv4(pkt.dst);
			if not ipv4_dst:
				logging.info('Destination inconnue : '+str(pkt.dst));
				return ;

			logging.info('>> IPv4='+ipv4_dst);

			# On associe le retour
			__TCP_RELAY__[ipv4_dst+str(pkt.dport)] = {'ip_src':pkt.src, 'sport': pkt.sport};

			# On récupère la partie TCP
			pkt_tcp = pkt[TCP];
			pkt_tcp.sport = __TCP_PORT__;
			pkt_tcp.chksum = None;
			del pkt_tcp.chksum;

			pk_final = IP(dst=ipv4_dst)/pkt_tcp;
			del pk_final[IP].chksum;
			del pk_final[IP].len;
			del pk_final[TCP].chksum;

			send(pk_final, iface="eth0", verbose=__LOGGING__ == logging.INFO);



def translate_IPv6_to_IPv4( ipv6 ):
	for ipv4 in __ASSOC__:
		if __ASSOC__[ipv4] == ipv6:
			return ipv4;
	return None;


# Système de log
def log():
	formatter = logging.Formatter( '[%(levelname)-8s][%(asctime)s][%(filename)s:%(lineno)3d] %(funcName)s() :: %(message)s', datefmt='%Y/%m/%d %H:%M' );

	ch = logging.StreamHandler();
	ch.setLevel( __LOGGING__ );
	ch.setFormatter( formatter );
	logging.getLogger().addHandler( ch );

	fh = logging.FileHandler( 'Error.log', 'w' );
	fh.setLevel( __LOGGING__ );
	fh.setFormatter( formatter );
	logging.getLogger().addHandler( fh );


log();
q = nfqueue.queue()
q.open()
q.unbind(socket.AF_INET6)
q.unbind(socket.AF_INET)
q.bind(socket.AF_INET6)
q.bind(socket.AF_INET)
q.set_callback(traite_paquet)
q.create_queue(0)
try:
	q.try_run()
except KeyboardInterrupt, e:
	print "interruption"
q.unbind(socket.AF_INET)
q.unbind(socket.AF_INET6)
q.close()
